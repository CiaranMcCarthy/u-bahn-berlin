# README #

## U-Bahn Berlin ##

This is a simple UWP app for off-line navigation of the Berlin U-Bahn system.

I used to own a Lumia 950XL with Windows 10 Mobile on it. Sometimes when I was travelling through the U-Bahn I would wonder when I'd reach my destination, if I was going to be on time or if I was going to be late. But when you're actually on the train it can be difficult to get a signal to check the BVG app.

So this app has the U-Bahn network, all stations, and the time it takes to travel between them.

There is also a simple Journey Planner in the app too. It's pretty incomplete, but should be fairly serviceable.

### How do I get set up? ###

The repository uses Git, so you can check it out with the usual Git commands.

Building and running the app requires Windows 10 (Creators Update), Visual Studio 2017 and the Windows 10 Creators Update (10.0; Build 15063) SDK installed on your machine.

Tests are written in NUnit so you will need the NUnit 3 Test Adapter add-in for Visual Studio: <https://marketplace.visualstudio.com/items?itemName=NUnitDevelopers.NUnit3TestAdapter>