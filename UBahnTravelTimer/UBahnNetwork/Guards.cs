﻿using System;
using System.Linq;

namespace UBahnNetwork
{
    internal static class Guards
    {
        private static readonly SupportedRoutes[] _allSupportedRoutes;

        static Guards()
        {
            _allSupportedRoutes = (SupportedRoutes[])Enum.GetValues(typeof(SupportedRoutes));
        }

        internal static void VerifyRouteSupported(SupportedRoutes route)
        {
            if (_allSupportedRoutes.Contains(route) == false)
            {
                throw new NotSupportedException(Properties.Resources.RouteNotSupported);
            }
        }

        internal static void StationsAreNotSame(Station firstStation, Station secondStation)
        {
            if (firstStation == secondStation)
            {
                throw new RouteBetweenSameStationsException(firstStation);
            }
        }

        internal static void StationIsPartOfRoute(Station station, Route route)
        {
            StationIsPartOfRoute(station, route.RouteNumber);
        }
        internal static void StationIsPartOfRoute(Station station, SupportedRoutes route)
        {
            if (station.Route.RouteNumber != route)
            {
                throw new ArgumentException(Properties.Resources.StationNotPartOfRoute, nameof(station));
            }
        }
    }
}