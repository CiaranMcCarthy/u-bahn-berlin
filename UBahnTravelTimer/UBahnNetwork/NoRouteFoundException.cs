﻿using System;
namespace UBahnNetwork
{
    public class NoRouteFoundException : Exception
    {
        public NoRouteFoundException(Station firstStation, Station secondStation)
            : base(Properties.Resources.NoRouteFoundException)
        {
            FirstStation = firstStation;
            SecondStation = secondStation;
        }

        public Station FirstStation { get; private set; }
        public Station SecondStation { get; private set; }
    }
}
