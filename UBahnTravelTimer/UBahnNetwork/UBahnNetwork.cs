﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UBahnNetwork.Journeys;
using UBahnNetwork.Links;

namespace UBahnNetwork
{
    public class UBahnNetwork
    {
        #region Static fields
        private static UBahnNetwork _uBahnNetwork;
        #endregion

        #region Fields
        private Dictionary<SupportedRoutes, Route> _routes = new Dictionary<SupportedRoutes, Route>();
        #endregion

        #region Constructor(s)
        #endregion

        #region Methods
        public static async Task InitialiseAsync()
        {
            if (_uBahnNetwork != null)
            {
                // Don't punish the user for making multiple calls to this method.
                return;
            }

            SupportedRoutes[] supportedRoutes = (SupportedRoutes[])Enum.GetValues(typeof(SupportedRoutes));
            Route[] routes = new Route[supportedRoutes.Length];

            for (int index = 0; index < supportedRoutes.Length; index++)
            {
                SupportedRoutes routeNumber = supportedRoutes[index];
                Route route = await Route.LoadAsync(routeNumber);
                routes[index] = route;
            }

            // If any stations have multiple routes, mark them here.
            // Don't do all items in the array - the last one will already be paired up with all others by the
            // time we get to it.
            for (int firstIndex = 0; firstIndex < routes.Length - 1; firstIndex++)
            {
                Route firstRoute = routes[firstIndex];
                foreach (Station firstStation in firstRoute.Stations)
                {
                    for (int secondIndex = firstIndex + 1; secondIndex < routes.Length; secondIndex++)
                    {
                        Route secondRoute = routes[secondIndex];
                        // If a station with the first name exists in the second route,
                        // create a link between the two.
                        Station secondStation = secondRoute.Stations.FirstOrDefault(i => i.Name == firstStation.Name);
                        if (secondStation != null)
                        {
                            ChangeRoutesLink link = new ChangeRoutesLink(firstStation, firstRoute.RouteNumber, secondStation, secondRoute.RouteNumber);
                            firstStation.LinkedRoutes.Add(link);
                            secondStation.LinkedRoutes.Add(link);
                        }
                    }
                }
            }



            _uBahnNetwork = new UBahnNetwork();
            foreach (Route route in routes)
            {
                _uBahnNetwork._routes.Add(route.RouteNumber, route);
            }
        }


        public async Task<Journey> GetJourneyAsync(string startingStationName, string destinationStationName)
        {
            return await Task.Run<Journey>(() =>
            {
                Dictionary<Station, Station> previous = new Dictionary<Station, Station>();
                Dictionary<Station, TimeSpan> distances = new Dictionary<Station, TimeSpan>();

                Station start = FindStation(startingStationName);
                Station destination = FindStation(destinationStationName);

                List<Station> knownNodes = new List<Station>();

                List<Station> unknownNodes = new List<Station>();
                foreach (Station station in AllStations)
                {
                    if (station.Name == start.Name)
                    {
                        distances[station] = TimeSpan.Zero;
                    }
                    else
                    {
                        distances[station] = TimeSpan.MaxValue;
                    }
                    unknownNodes.Add(station);
                }



                while (unknownNodes.Count > 0)
                {
                    Station nearestStation = unknownNodes.OrderBy(i => distances[i]).First();
                    unknownNodes.Remove(nearestStation);
                    if (nearestStation.Name == destination.Name)
                    {
                        List<Station> path = new List<Station>();
                        if (previous.ContainsKey(nearestStation) == false)
                        {
                            // Cannot find a route - give up here...
                            throw new NoRouteFoundException(start, destination);
                        }
                        while (previous.ContainsKey(nearestStation))
                        {
                            path.Add(nearestStation);
                            nearestStation = previous[nearestStation];
                        }
                        path.Add(start);

                        path.Reverse();
                        return new Journey(path.ToArray());
                    }

                    foreach (ILink link in nearestStation.Links)
                    {
                        Station neighbour = link.GetNeighbour(nearestStation);
                        // Cannot find a route - give up here...
                        if (distances[nearestStation] == TimeSpan.MaxValue)
                        {
                            throw new NoRouteFoundException(start, destination);
                        }
                        TimeSpan alt = distances[nearestStation] + link.LinkTime;
                        if (alt < distances[neighbour])
                        {
                            distances[neighbour] = alt;
                            previous[neighbour] = nearestStation;
                        }
                    }
                }
                throw new NoRouteFoundException(start, destination);
            });
        }

        private void EvaluateNeighbours(Station currentNode, List<Station> settledNodes, Dictionary<Station, TimeSpan> unsettledNodes, Dictionary<Station, TimeSpan> distances)
        {
            foreach (ILink link in currentNode.Links)
            {
                Station linkedStation = link.GetNeighbour(currentNode);
                if (settledNodes.Contains(linkedStation))
                {
                    continue;
                }


                TimeSpan travelTime = link.LinkTime;
                TimeSpan totalTravelTimeSoFar = distances[currentNode] + travelTime;

                if (distances.ContainsKey(linkedStation) == false || totalTravelTimeSoFar < distances[linkedStation])
                {
                    distances[linkedStation] = totalTravelTimeSoFar;
                }
                if (unsettledNodes.ContainsKey(linkedStation) == false)
                {
                    unsettledNodes[linkedStation] = totalTravelTimeSoFar;
                }
            }
        }

        private Station FindStation(string stationName)
        {
            return AllStations.First(i => i.Name == stationName);
        }
        #endregion

        #region Properties
        public IEnumerable<Route> AllRoutes
        {
            get
            {
                return _routes.Values;
            }
        }

        public IEnumerable<Station> AllStations
        {
            get
            {
                foreach (Route route in _routes.Values)
                {
                    foreach (Station station in route.Stations)
                    {
                        yield return station;
                    }
                }
            }
        }

        public Route this[SupportedRoutes routeNumber]
        {
            get
            {
                return _routes[routeNumber];
            }
        }

        public static UBahnNetwork Network
        {
            get
            {
                if (_uBahnNetwork == null)
                {
                    throw new InvalidOperationException(Properties.Resources.UBahnNetworkNotInitialised);
                }
                return _uBahnNetwork;
            }
        }
        #endregion
    }
}