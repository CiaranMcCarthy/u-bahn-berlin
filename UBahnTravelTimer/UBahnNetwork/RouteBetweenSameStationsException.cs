﻿using System;

namespace UBahnNetwork
{
    public class RouteBetweenSameStationsException : Exception
    {
        public RouteBetweenSameStationsException(Station station)
            : base(Properties.Resources.RouteBetweenSameStationsException)
        {
            Station = station;
        }

        public Station Station { get; private set; }
    }
}
