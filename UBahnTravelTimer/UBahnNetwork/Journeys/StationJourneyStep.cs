﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UBahnNetwork.Journeys
{
    public class StationJourneyStep : IJourneyStep
    {
        #region Properties
        public TimeSpan? Duration { get; set; }
        public Station Station { get; set; }
        #endregion
    }
}
