﻿using System;

namespace UBahnNetwork.Journeys
{
    public interface IJourneyStep
    {
        TimeSpan? Duration { get; }
        Station Station { get; set; }
    }
}
