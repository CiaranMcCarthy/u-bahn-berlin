﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UBahnNetwork.Journeys
{
    public class RouteJourneyStep : IJourneyStep
    {
        #region Properties
        public TimeSpan? Duration { get; set; }
        public Station Station { get; set; }

        public Route StartingRoute { get; set; }
        public Route EndingRoute { get; set; }
        #endregion
    }
}
