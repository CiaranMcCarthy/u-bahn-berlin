﻿using System;
using UBahnNetwork.Links;

namespace UBahnNetwork.Journeys
{
    public class Journey
    {
        public Journey(Station[] path)
        {
            Steps = new IJourneyStep[path.Length];

            for (int index = 0; index < path.Length; index++)
            {
                Station currentStation = path[index];
                ILink link = null;
                if (index < path.Length - 1)
                {
                    Station nextStation = path[index + 1];
                    foreach (ILink currentLink in currentStation.Links)
                    {
                        if (currentLink.GetNeighbour(currentStation).Name == nextStation.Name)
                        {
                            link = currentLink;
                            break;
                        }
                    }

                    if (link == null)
                    {
                        throw new NullReferenceException(nameof(link) + " was null, when it was not supposed to be.");
                    }
                }

                IJourneyStep nextStep;
                if (link is ChangeStationsLink)
                {
                    ChangeStationsLink changeStationsLink = (ChangeStationsLink)link;
                    nextStep = new StationJourneyStep()
                    {
                        Station = currentStation,
                        Duration = changeStationsLink.LinkTime
                    };
                }
                else if (link is ChangeRoutesLink)
                {
                    ChangeRoutesLink changeRoutesLink = (ChangeRoutesLink)link;
                    nextStep = new RouteJourneyStep()
                    {
                        StartingRoute = currentStation.Route,
                        EndingRoute = changeRoutesLink.GetNeighbour(currentStation).Route,
                        Duration = changeRoutesLink.LinkTime,
                        Station = currentStation
                    };
                }
                else
                {
                    // Link is null. We've come to the end of the journey.
                    nextStep = new StationJourneyStep()
                    {
                        Station = currentStation,
                        Duration = null
                    };
                }

                Steps[index] = nextStep;
            }
        }

        #region Properties
        public IJourneyStep[] Steps { get; private set; }

        public TimeSpan Duration
        {
            get
            {
                TimeSpan time = new TimeSpan();
                foreach (IJourneyStep step in Steps)
                {
                    if (step.Duration != null)
                    {
                        time = time.Add(step.Duration.Value);
                    }
                }
                return time;
            }
        }
        #endregion
    }
}
