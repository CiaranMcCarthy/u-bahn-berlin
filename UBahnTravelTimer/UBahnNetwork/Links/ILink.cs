﻿using System;

namespace UBahnNetwork.Links
{
    public interface ILink
    {
        TimeSpan LinkTime { get; }
        Station GetNeighbour(Station current);
    }
}