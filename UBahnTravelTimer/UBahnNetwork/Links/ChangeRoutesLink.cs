﻿using System;

namespace UBahnNetwork.Links
{
    public class ChangeRoutesLink : ILink
    {
        public TimeSpan LinkTime
        {
            get
            {
                return TimeSpan.FromMinutes(10);
            }
        }

        public SupportedRoutes RouteA { get; private set; }
        public Station StationA { get; private set; }
        public SupportedRoutes RouteB { get; private set; }
        public Station StationB { get; private set; }

        public Station GetNeighbour(Station current)
        {
            if (StationA == current)
            {
                return StationB;
            }
            else
            {
                return StationA;
            }
        }

        public SupportedRoutes GetNeighbouringRoute(SupportedRoutes current)
        {
            if (RouteA == current)
            {
                return RouteB;
            }
            else
            {
                return RouteA;
            }
        }

        public ChangeRoutesLink(Station stationA, SupportedRoutes routeA, Station stationB, SupportedRoutes routeB)
        {
            StationA = stationA;
            RouteA = routeA;

            StationB = stationB;
            RouteB = routeB;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} <-> {2} {3})", RouteA, StationA.Name, RouteB, StationB);
        }
    }
}
