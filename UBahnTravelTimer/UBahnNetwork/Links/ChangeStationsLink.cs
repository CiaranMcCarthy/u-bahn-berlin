﻿using System;

namespace UBahnNetwork.Links
{
    public class ChangeStationsLink : ILink
    {
        public TimeSpan LinkTime { get; private set; }

        public Station StationA { get; private set; }
        public Station StationB { get; private set; }

        public Station GetNeighbour(Station current)
        {
            if (StationA == current)
            {
                return StationB;
            }
            else
            {
                return StationA;
            }
        }

        public ChangeStationsLink(Station stationA, Station stationB, TimeSpan travelTime)
        {
            StationA = stationA;
            StationB = stationB;
            LinkTime = travelTime;
        }

        public override string ToString()
        {
            return string.Format("{0} <- {1} minutes -> {2}", StationA.Name, LinkTime.TotalMinutes, StationB.Name);
        }
    }
}
