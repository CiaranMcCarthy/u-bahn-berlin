﻿namespace UBahnNetwork
{
    public enum SupportedRoutes
    {
        U1 = 1,
        U2 = 2,
        U3 = 3,
        U4 = 4,
        U5 = 5,
        U6 = 6,
        U7 = 7,
        U8 = 8,
        U9 = 9,
        U55 = 55,
        //U12 = 12,
    }
}
