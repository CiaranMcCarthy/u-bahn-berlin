﻿using System.Collections.Generic;
using System.Linq;
using UBahnNetwork.Links;

namespace UBahnNetwork
{
    public class Station
    {
        public string Name { get; private set; }
        public Route Route { get; internal set; }
        public ChangeStationsLink ForwardLink { get; internal set; }
        public ChangeStationsLink BackwardLink { get; internal set; }

        public List<ChangeRoutesLink> LinkedRoutes { get; private set; }
        public IEnumerable<ILink> Links
        {
            get
            {
                List<ILink> routeLinks = LinkedRoutes.Select(i => i as ILink).ToList();
                if (ForwardLink != null)
                {
                    routeLinks.Add(ForwardLink);
                }
                if (BackwardLink != null)
                {
                    routeLinks.Add(BackwardLink);
                }
                return routeLinks;
            }
        }

        public ILink NearestLink
        {
            get
            {
                ILink minimum = null;
                foreach (ILink link in Links)
                {
                    if (minimum == null)
                    {
                        minimum = link;
                    }
                    else if (minimum.LinkTime > link.LinkTime)
                    {
                        minimum = link;
                    }
                }
                return minimum;
            }
        }

        public Station(string name)
        {
            Name = name;
            LinkedRoutes = new List<ChangeRoutesLink>();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
