﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using System.Reflection;
using System.Linq;
using UBahnNetwork.Links;

namespace UBahnNetwork
{
    public class Route
    {
        #region Fields
        private const char _separatorCharacter = ';';
        #endregion

        #region Properties
        public Station[] Stations { get; private set; }
        public SupportedRoutes RouteNumber { get; private set; }

        public string FirstColourHex { get; private set; }
        public string SecondColourHex { get; private set; }
        public string ForegroundColourHex { get; private set; }

        public TimeSpan RouteLength
        {
            get
            {
                return TravelTimeBetweenStations(Stations.First(), Stations.Last());
            }
        }
        #endregion

        #region Methods
        public static async Task<Route> LoadAsync(SupportedRoutes route)
        {
            Guards.VerifyRouteSupported(route);

            List<Station> stations = new List<Station>();
            List<int> connectionTimes = new List<int>();
            string firstColourHex;
            string secondColourHex;

            string foregroundColourHex;

            var assembly = typeof(Route).GetTypeInfo().Assembly;
            using (Stream fileStream = assembly.GetManifestResourceStream("UBahnNetwork.RouteData." + route + ".txt"))
            using (StreamReader textStream = new StreamReader(fileStream))
            {
                string nextLine = await textStream.ReadLineAsync();
                ExtractColours(nextLine, out firstColourHex, out secondColourHex);
                nextLine = await textStream.ReadLineAsync();
                foregroundColourHex = nextLine;

                while (textStream.EndOfStream == false)
                {
                    nextLine = await textStream.ReadLineAsync();
                    string stationName;
                    int? connectionTimeInMinutes;
                    if (nextLine.Contains("" + _separatorCharacter))
                    {
                        string[] splits = nextLine.Split(_separatorCharacter);
                        stationName = splits[0];
                        connectionTimeInMinutes = int.Parse(splits[1], CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        // No link time - station is terminus.
                        stationName = nextLine;
                        connectionTimeInMinutes = null;
                    }

                    Station nextStation = new Station(stationName);
                    stations.Add(nextStation);
                    if (connectionTimeInMinutes != null)
                    {
                        connectionTimes.Add(connectionTimeInMinutes.Value);
                    }
                }
            }

            Route newRoute = new Route();
            newRoute.RouteNumber = route;
            newRoute.FirstColourHex = firstColourHex;
            newRoute.SecondColourHex = secondColourHex;
            newRoute.ForegroundColourHex = foregroundColourHex;
            newRoute.Stations = stations.ToArray();
            for (int index = 0; index < stations.Count; index++)
            {
                Station currentStation = stations[index];
                currentStation.Route = newRoute;

                if (index > 0)
                {
                    Station previousStation = newRoute.Stations[index - 1];
                    currentStation.ForwardLink = previousStation.BackwardLink;
                }
                if (index < stations.Count - 1)
                {
                    Station nextStation = newRoute.Stations[index + 1];
                    currentStation.BackwardLink = new ChangeStationsLink(currentStation, nextStation, TimeSpan.FromMinutes(connectionTimes[index]));
                }
            }
            
            return newRoute;
        }

        private static void ExtractColours(string nextLine, out string firstColourHex, out string secondColourHex)
        {
            if (nextLine.Contains("" + _separatorCharacter))
            {
                string[] splits = nextLine.Split(_separatorCharacter);
                firstColourHex = splits[0];
                secondColourHex = splits[1];
            }
            else
            {
                firstColourHex = nextLine;
                secondColourHex = nextLine;
            }
        }

        public TimeSpan TravelTimeBetweenStations(Station firstStation, Station secondStation)
        {
            Guards.StationsAreNotSame(firstStation, secondStation);
            Guards.StationIsPartOfRoute(firstStation, this);
            Guards.StationIsPartOfRoute(secondStation, this);

            TimeSpan travelTime;
            if (TrySearchForwards(firstStation, secondStation, out travelTime))
            {
                return travelTime;
            }
            else if (TrySearchBackwards(firstStation, secondStation, out travelTime))
            {
                return travelTime;
            }
            else
            {
                throw new NoRouteFoundException(firstStation, secondStation);
            }
        }

        private bool TrySearchBackwards(Station firstStation, Station secondStation, out TimeSpan time)
        {
            time = new TimeSpan();
            Station currentStation = firstStation;
            do
            {
                ChangeStationsLink nextLink = currentStation.ForwardLink;
                if (nextLink == null)
                {
                    return false;
                }
                time += nextLink.LinkTime;
                currentStation = nextLink.StationA;
            }
            while (currentStation != secondStation);

            return true;
        }

        private bool TrySearchForwards(Station firstStation, Station secondStation, out TimeSpan time)
        {
            Station currentStation = firstStation;
            do
            {
                ChangeStationsLink nextLink = currentStation.BackwardLink;
                if (nextLink == null)
                {
                    return false;
                }
                time += nextLink.LinkTime;
                currentStation = nextLink.StationB;
            }
            while (currentStation != secondStation);

            return true;
        }

        public override string ToString()
        {
            return RouteNumber.ToString();
        }
        #endregion
    }
}