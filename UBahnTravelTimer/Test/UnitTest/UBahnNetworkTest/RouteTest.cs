﻿using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using UBahnNetwork;

namespace UBahnNetworkTest
{
    [TestFixture]
    public class RouteTest
    {
        [TestCase(SupportedRoutes.U1, 20)]
        [TestCase(SupportedRoutes.U2, 47)]
        [TestCase(SupportedRoutes.U3, 24)]
        [TestCase(SupportedRoutes.U4, 6)]
        [TestCase(SupportedRoutes.U5, 32)]
        [TestCase(SupportedRoutes.U6, 38)]
        [TestCase(SupportedRoutes.U7, 56)]
        [TestCase(SupportedRoutes.U8, 36)]
        [TestCase(SupportedRoutes.U9, 23)]
        [TestCase(SupportedRoutes.U55, 2)]
        //[TestCase(SupportedRoutes.U12, 35)]
        public async Task RouteLength_GetRouteLength_ResultMatchesExpected(SupportedRoutes routeNumber, int expectedTimeInMinutes)
        {
            // Arrange
            Route route = await Route.LoadAsync(routeNumber);

            // Act
            TimeSpan actualTime = route.RouteLength;

            // Assert
            Assert.That(actualTime.TotalMinutes, Is.EqualTo(expectedTimeInMinutes));
        }

        [TestCase("Südstern", "Hermannplatz", 2)]
        [TestCase("Hermannplatz", "Südstern", 2)]
        [TestCase("Siemensdamm", "Konstanzer Straße", 12)]
        [TestCase("Konstanzer Straße", "Siemensdamm", 12)]
        public async Task TravelTimeBetweenStations_RouteIsU7_StationTimesCorrectlyReturned(string departureStationName, string destinationStationName, int expectedTimeInMinutes)
        {
            // Arrange
            const SupportedRoutes routeNumber = SupportedRoutes.U7;
            Route route = await Route.LoadAsync(routeNumber);

            // Act
            Station departureStation = route.Stations.First(i => i.Name == departureStationName);
            Station destinationStation = route.Stations.First(i => i.Name == destinationStationName);
            TimeSpan actualTime = route.TravelTimeBetweenStations(departureStation, destinationStation);

            // Assert
            Assert.That(actualTime.TotalMinutes, Is.EqualTo(expectedTimeInMinutes));
        }

        [Test]
        public async Task TravelTimeBetweenStations_SameStationUsed_ThrowsRouteBetweenSameStationsException()
        {
            // Arrange
            const string stationName = "Rudow";
            const SupportedRoutes routeNumber = SupportedRoutes.U7;
            Route route = await Route.LoadAsync(routeNumber);

            // Act
            Station departureStation = route.Stations.First(i => i.Name == stationName);
            Station destinationStation = route.Stations.First(i => i.Name == stationName);
            TestDelegate actDelegate = () => route.TravelTimeBetweenStations(departureStation, destinationStation);

            // Assert
            Assert.That(actDelegate, Throws.InstanceOf<RouteBetweenSameStationsException>());
        }

        [Test]
        public async Task TravelTimeBetweenStations_StartingStationIsNotPartOfThisRoute_ThrowsArgumentException()
        {
            // Arrange
            const string departureStationName = "Bundestag";
            const string destinationStationName = "Rudow";
            const SupportedRoutes routeNumber = SupportedRoutes.U7;
            const SupportedRoutes otherRouteNumber = SupportedRoutes.U55;
            Route route = await Route.LoadAsync(routeNumber);
            Route otherRoute = await Route.LoadAsync(otherRouteNumber);

            // Act
            Station departureStation = otherRoute.Stations.First(i => i.Name == departureStationName);
            Station destinationStation = route.Stations.First(i => i.Name == destinationStationName);
            TestDelegate actDelegate = () => route.TravelTimeBetweenStations(departureStation, destinationStation);

            // Assert
            Assert.That(actDelegate, Throws.ArgumentException);
        }

        [Test]
        public async Task TravelTimeBetweenStations_DestinationStationIsNotPartOfThisRoute_ThrowsArgumentException()
        {
            // Arrange
            const string departureStationName = "Rudow";
            const string destinationStationName = "Bundestag";
            const SupportedRoutes routeNumber = SupportedRoutes.U7;
            const SupportedRoutes otherRouteNumber = SupportedRoutes.U55;
            Route route = await Route.LoadAsync(routeNumber);
            Route otherRoute = await Route.LoadAsync(otherRouteNumber);

            // Act
            Station departureStation = route.Stations.First(i => i.Name == departureStationName);
            Station destinationStation = otherRoute.Stations.First(i => i.Name == destinationStationName);
            TestDelegate actDelegate = () => route.TravelTimeBetweenStations(departureStation, destinationStation);

            // Assert
            Assert.That(actDelegate, Throws.ArgumentException);
        }
    }
}