﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UBahnNetwork;
using UBahnNetwork.Journeys;

namespace UBahnNetworkTest
{
    [TestFixture]
    public class UBahnNetworkTest
    {
        [OneTimeSetUp]
        public async Task OneTimeSetUp()
        {
            // Initialise the U-Bahn Network.
            await UBahnNetwork.UBahnNetwork.InitialiseAsync();
        }

        [Test]
        public void StaticInstance_CreatedWithAllSupportedRoutes_AllSupportedRoutesInNetwork()
        {
            // Arrange
            SupportedRoutes[] allExpectedRoutes = (SupportedRoutes[])Enum.GetValues(typeof(SupportedRoutes));

            // Act
            IEnumerable<SupportedRoutes> allActualRoutes = UBahnNetwork.UBahnNetwork.Network.AllRoutes.Select(i => i.RouteNumber);

            //Assert
            Assert.That(allActualRoutes, Is.EquivalentTo(allExpectedRoutes));
        }

        #region GetJourney
        [Test]
        public async Task GetJourney_GetsJourneyBetweenTwoStationsOnSameLine_GoesBackwards_JourneyAndTimeMatchExpected()
        {
            // Arrange
            string startingStationName = "Konstanzer Straße";
            string destinationStationName = "Siemensdamm";

            string[] expectedStationNames = new string[]
            {
                "Konstanzer Straße",
                "Adenauerplatz",
                "Wilmersdorfer Straße",
                "Bismarkstraße",
                "Richard-Wagner-Platz",
                "Mierendorffplatz",
                "Jungfernheide",
                "Jakob-Kaiser-Platz",
                "Halemweg",
                "Siemensdamm"
            };
            const int expectedDurationInMinutes = 12;

            // Act
            Journey journey = await UBahnNetwork.UBahnNetwork.Network.GetJourneyAsync(startingStationName, destinationStationName);

            // Assert
            Assert.That(journey.Steps.Select(i => i.Station.Name), Is.EqualTo(expectedStationNames));
            Assert.That(journey.Duration.TotalMinutes, Is.EqualTo(expectedDurationInMinutes));
        }

        [Test]
        public async Task GetJourney_GetsJourneyBetweenTwoStationsOnSameLine_GoesForwards_JourneyAndTimeMatchExpected()
        {
            // Arrange
            string startingStationName = "Schwartzkopffstraße";
            string destinationStationName = "Tempelhof";

            string[] expectedStationNames = new string[]
            {
                "Schwartzkopffstraße",
                "Naturkundemuseum",
                "Oranienburger Tor",
                "Friedrichstraße",
                "Französische Straße",
                "Stadtmitte",
                "Kockstraße/Checkpoint Charlie",
                "Hallesches Tor",
                "Mehringdamm",
                "Platz der Luftbrücke",
                "Paradestraße",
                "Tempelhof"
            };
            const int expectedDurationInMinutes = 15;

            // Act
            Journey journey = await UBahnNetwork.UBahnNetwork.Network.GetJourneyAsync(startingStationName, destinationStationName);

            // Assert
            Assert.That(journey.Steps.Select(i => i.Station.Name), Is.EqualTo(expectedStationNames));
            Assert.That(journey.Duration.TotalMinutes, Is.EqualTo(expectedDurationInMinutes));
        }

        [Test]
        public async Task GetJourney_GetsJourneyBetweenTwoStationsOnTwoDifferentLines_JourneyAndTimeMatchExpected()
        {
            // Arrange
            string startingStationName = "Kottbusser Tor";
            string destinationStationName = "Deutsche Oper";

            string[] expectedStationNames = new string[]
            {
                "Kottbusser Tor",
                "Prinzenstraße",
                "Hallesches Tor",
                "Möckernbrücke",
                "Gleisdreieck",
                "Kurfürstenstraße",
                "Nollendorfplatz",
                "Nollendorfplatz",
                "Wittenbergplatz",
                "Zoologischer Garten",
                "Ernst-Reuter-Platz",
                "Deutsche Oper"
            };
            const int expectedDurationInMinutes = 28;

            // Act
            Journey journey = await UBahnNetwork.UBahnNetwork.Network.GetJourneyAsync(startingStationName, destinationStationName);

            // Assert
            Assert.That(journey.Steps.Select(i => i.Station.Name), Is.EqualTo(expectedStationNames));
            Assert.That(journey.Duration.TotalMinutes, Is.EqualTo(expectedDurationInMinutes));
        }

        [Test]
        public async Task GetJourney_GetsJourneyBetweenTwoStationsOnThreeDifferentLines_JourneyAndTimeMatchExpected()
        {
            // Arrange
            string startingStationName = "Otisstraße";
            string destinationStationName = "Innsbrucker Platz";

            string[] expectedStationNames = new string[]
            {
                "Otisstraße",
                "Scharnweberstraße",
                "Kurt-Schumacher-Platz",
                "Afrikanische Straße",
                "Rehberge",
                "Seestraße",
                "Leopoldplatz",
                "Wedding",
                "Reinickendorfer Straße",
                "Schwartzkopffstraße",
                "Naturkundemuseum",
                "Oranienburger Tor",
                "Friedrichstraße",
                "Französische Straße",
                "Stadtmitte",
                "Kockstraße/Checkpoint Charlie",
                "Hallesches Tor",
                "Mehringdamm",
                "Mehringdamm",
                "Möckernbrücke",
                "Yorckstraße",
                "Kleistpark",
                "Eisenacher Straße",
                "Bayerischer Platz",
                "Bayerischer Platz",
                "Rathaus Schöneberg",
                "Innsbrucker Platz"
            };
            const int expectedDurationInMinutes = 54;

            // Act
            Journey journey = await UBahnNetwork.UBahnNetwork.Network.GetJourneyAsync(startingStationName, destinationStationName);

            // Assert
            Assert.That(journey.Steps.Select(i => i.Station.Name), Is.EqualTo(expectedStationNames));
            Assert.That(journey.Duration.TotalMinutes, Is.EqualTo(expectedDurationInMinutes));
        }

        [TestCase("Ruhleben", "Bundestag")]
        [TestCase("Uhlandstraße", "Brandenburger Tor")]
        public void GetJourney_CannotFindARoute_ThrowsRouteNotFoundException(string startingStationName, string destinationStationName)
        {
            // Arrange

            // Act
            AsyncTestDelegate actDelegate = async () => await UBahnNetwork.UBahnNetwork.Network.GetJourneyAsync(startingStationName, destinationStationName);

            // Assert
            Assert.That(actDelegate, Throws.InstanceOf<NoRouteFoundException>());
        }
        [Test]
        public async Task GetJourney_GetsJourneyBetweenTwoStationsOnSameLine_StartingStationCanBeOnDifferentLine_JourneyAndTimeMatchExpected()
        {
            // Arrange
            string startingStationName = "Nollendorfplatz";
            string destinationStationName = "Fehrbelliner Platz";

            string[] expectedStationNames = new string[]
            {
                "Nollendorfplatz",
                "Wittenbergplatz",
                "Augsburger Straße",
                "Spichernstraße",
                "Hohenzollernplatz",
                "Fehrbelliner Platz"
            };
            const int expectedDurationInMinutes = 8;

            // Act
            Journey journey = await UBahnNetwork.UBahnNetwork.Network.GetJourneyAsync(startingStationName, destinationStationName);

            // Assert
            Assert.That(journey.Steps.Select(i => i.Station.Name), Is.EqualTo(expectedStationNames));
            Assert.That(journey.Duration.TotalMinutes, Is.EqualTo(expectedDurationInMinutes));
        }
        #endregion
    }
}
