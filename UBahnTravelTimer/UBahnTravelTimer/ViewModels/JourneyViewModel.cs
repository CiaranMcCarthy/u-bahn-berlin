﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using UBahnNetwork;
using UBahnNetwork.Journeys;
using UBahnTravelTimer.Commands;

namespace UBahnTravelTimer.ViewModels
{
    public class JourneyViewModel : IErrorViewModel
    {
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Fields
        private static readonly SupportedRoutes[] _availableRoutes;

        private SupportedRoutes _fromRoute;
        private SupportedRoutes _toRoute;
        private Station _fromStation;
        private Station _toStation;
        private Journey _journey;
        private Exception _error;
        #endregion

        #region Constructor(s)
        static JourneyViewModel()
        {
            _availableRoutes = (SupportedRoutes[])Enum.GetValues(typeof(SupportedRoutes));
        }

        public JourneyViewModel()
        {
            FindJourneyCommand = new SimpleAsyncCommand(PreExecute, FindJourneyAsync, PostExecute);
            FromRoute = _availableRoutes.First();
            ToRoute = _availableRoutes.First();
        }
        #endregion

        #region Methods
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void PreExecute()
        {
            //IsBusy = true;
            Error = null;
        }

        private void PostExecute(Task task)
        {
            if (task.IsFaulted)
            {
                if (task.Exception is AggregateException)
                {
                    AggregateException aggregateException = (AggregateException)task.Exception;
                    aggregateException = aggregateException.Flatten();
                    Error = aggregateException.InnerExceptions.First();
                }
                else
                {
                    Error = task.Exception;
                }
            }
            //IsBusy = false;
        }
        #endregion

        #region Commands
        public ICommand FindJourneyCommand
        {
            get; private set;
        }

        public async Task FindJourneyAsync()
        {
            Journey = await UBahnNetwork.UBahnNetwork.Network.GetJourneyAsync(FromStation.Name, ToStation.Name);
        }
        #endregion

        #region Properties
        public SupportedRoutes FromRoute
        {
            get
            {
                return _fromRoute;
            }
            set
            {
                if (_fromRoute != value)
                {
                    _fromRoute = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(AvailableFromStations));
                    FromStation = AvailableFromStations.First();
                }
            }
        }
        public SupportedRoutes ToRoute
        {
            get
            {
                return _toRoute;
            }
            set
            {
                if (_toRoute != value)
                {
                    _toRoute = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(AvailableToStations));
                    ToStation = AvailableToStations.First();
                }
            }
        }

        public Station FromStation
        {
            get
            {
                return _fromStation;
            }
            set
            {
                if (_fromStation != value)
                {
                    _fromStation = value;
                    OnPropertyChanged();
                }
            }
        }

        public Station ToStation
        {
            get
            {
                return _toStation;
            }
            set
            {
                if (_toStation != value)
                {
                    _toStation = value;
                    OnPropertyChanged();
                }
            }
        }

        public SupportedRoutes[] AvailableRoutes
        {
            get
            {
                return _availableRoutes;
            }
        }

        public Station[] AvailableFromStations
        {
            get
            {
                return UBahnNetwork.UBahnNetwork.Network[FromRoute].Stations;
            }
        }

        public Station[] AvailableToStations
        {
            get
            {
                return UBahnNetwork.UBahnNetwork.Network[ToRoute].Stations;
            }
        }

        public Journey Journey
        {
            get
            {
                return _journey;
            }
            set
            {
                if (_journey != value)
                {
                    _journey = value;
                    OnPropertyChanged();
                }
            }
        }

        public Exception Error
        {
            get
            {
                return _error;
            }
            set
            {
                if (_error != value)
                {
                    _error = value;
                    OnPropertyChanged();
                }
            }
        }
        #endregion
    }
}
