﻿// Only compile this class if we're in debug mode (Designer model).
#if DEBUG
using UBahnNetwork;

namespace UBahnTravelTimer.ViewModels.Designer
{
    public class DesignerRouteViewModel : RouteViewModel
    {
        public DesignerRouteViewModel() : base(LoadRoute())
        {
        }

        private static Route LoadRoute()
        {
            return Route.LoadAsync(SupportedRoutes.U2).Result;
        }
    }
}
#endif