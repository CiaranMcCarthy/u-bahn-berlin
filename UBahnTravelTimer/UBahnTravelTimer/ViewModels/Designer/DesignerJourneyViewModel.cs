﻿// Only compile this class if we're in debug mode (Designer model).
#if DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBahnNetwork;

namespace UBahnTravelTimer.ViewModels.Designer
{
    public class DesignerJourneyViewModel : JourneyViewModel
    {
        public DesignerJourneyViewModel()
        {
            FromRoute = SupportedRoutes.U7;
            ToRoute = SupportedRoutes.U8;
            FromStation = new Station("Wilmersdorferstr");
            ToStation = new Station("Hermannplatz");
        }
    }
}
#endif