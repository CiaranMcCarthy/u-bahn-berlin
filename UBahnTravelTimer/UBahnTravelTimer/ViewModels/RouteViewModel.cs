﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UBahnNetwork;
using Windows.UI;
using Windows.UI.Xaml.Media;

namespace UBahnTravelTimer.ViewModels
{
    public class RouteViewModel : INotifyPropertyChanged
    {
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Fields
        private Station _firstSelectedStation;
        private Station _secondSelectedStation;
        #endregion

        #region Constructors
        public RouteViewModel(Route route)
        {
            Route = route;
        }
        #endregion

        #region Properties
        public Route Route { get; private set; }

        public Brush FirstColourBrush
        {
            get
            {
                return GetRouteColorBrush(Route.FirstColourHex);
            }
        }

        public Brush SecondColourBrush
        {
            get
            {
                return GetRouteColorBrush(Route.SecondColourHex);
            }
        }

        public Brush ForegroundColourBrush
        {
            get
            {
                return GetRouteColorBrush(Route.ForegroundColourHex);
            }
        }

        public Station FirstSelectedStation
        {
            get
            {
                return _firstSelectedStation;
            }
            set
            {
                if (_firstSelectedStation != value)
                {
                    _firstSelectedStation = value;
                    OnPropertyChanged();
                    UpdateTimes();
                }
            }
        }
        public Station SecondSelectedStation
        {
            get
            {
                return _secondSelectedStation;
            }
            set
            {
                if (_secondSelectedStation != value)
                {
                    _secondSelectedStation = value;
                    OnPropertyChanged();
                    UpdateTimes();
                }
            }
        }

        public TimeSpan? TimeBetweenStations
        {
            get
            {
                if (FirstSelectedStation == null || SecondSelectedStation == null || FirstSelectedStation == SecondSelectedStation)
                {
                    return null;
                }
                else
                {
                    return Route.TravelTimeBetweenStations(FirstSelectedStation, SecondSelectedStation);
                }
            }
        }

        public DateTime? ArrivalTime
        {
            get
            {
                if (FirstSelectedStation == null || SecondSelectedStation == null || FirstSelectedStation == SecondSelectedStation)
                {
                    return null;
                }
                else
                {
                    TimeSpan travelTime = Route.TravelTimeBetweenStations(FirstSelectedStation, SecondSelectedStation);
                    return DateTime.Now + travelTime;
                }
            }
        }
        #endregion

        #region Methods
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public static SolidColorBrush GetRouteColorBrush(string colourHex)
        {
            Color colour = ColourFromHexString(colourHex);
            SolidColorBrush myBrush = new SolidColorBrush(colour);
            return myBrush;
        }

        private static Color ColourFromHexString(string hex)
        {
            hex = hex.Replace("#", string.Empty);
            hex = hex.Replace("0x", string.Empty);
            byte a = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
            byte r = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
            byte g = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));
            byte b = (byte)(Convert.ToUInt32(hex.Substring(6, 2), 16));
            return Color.FromArgb(a, r, g, b);
        }

        private void UpdateTimes()
        {
            OnPropertyChanged(nameof(TimeBetweenStations));
            OnPropertyChanged(nameof(ArrivalTime));
        }
        #endregion
    }
}
