﻿using System;
using System.ComponentModel;

namespace UBahnTravelTimer.ViewModels
{
    public interface IErrorViewModel : INotifyPropertyChanged
    {
        Exception Error { get; }
    }
}
