﻿using UBahnNetwork;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace UBahnTravelTimer.Controls
{
    public sealed partial class StopControl : UserControl
    {
        public static readonly DependencyProperty IsStationSelectedAsSourceProperty = DependencyProperty.RegisterAttached("IsStationSelectedAsSource", typeof(bool?), typeof(StopControl), new PropertyMetadata(null, IsStationSelectedAsSourceProperty_PropertyChanged));
        private static void IsStationSelectedAsSourceProperty_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StopControl major = (StopControl)d;
            bool? checkedValue = (bool?)e.NewValue;
            major.chkSource.IsChecked = checkedValue;
        }

        public StopControl()
        {
            this.InitializeComponent();
        }

        public Station Station
        {
            get { return DataContext as Station; }
        }

        public bool? IsStationSelectedAsSource
        {
            get { return (bool?)GetValue(IsStationSelectedAsSourceProperty); }
            set { SetValue(IsStationSelectedAsSourceProperty, value); }
        }

        public bool IsLast
        {
            get
            {
                return (RectangleBottom.Visibility != Visibility.Visible);
            }
            set
            {
                RectangleBottom.Visibility = (value) ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public Brush FirstRouteBrush
        {
            get
            {
                return RectangleTop.Fill;
            }
            set
            {
                RectangleTop.Fill = value;
            }
        }

        public Brush SecondRouteBrush
        {
            get
            {
                return RectangleBottom.Fill;
            }
            set
            {
                RectangleBottom.Fill = value;
            }
        }

        public bool? IsCheckedAsSource
        {
            get
            {
                return chkSource.IsChecked;
            }
            set
            {
                chkSource.IsChecked = value;
            }
        }

        public bool? IsCheckedAsDestination
        {
            get
            {
                return chkDestination.IsChecked;
            }
            set
            {
                chkDestination.IsChecked = value;
            }
        }

        public event RoutedEventHandler StationCheckedAsSource;
        public event RoutedEventHandler StationCheckedAsDestination;
        public event RoutedEventHandler StationUncheckedAsSource;
        public event RoutedEventHandler StationUncheckedAsDestination;

        private void chkSource_Checked(object sender, RoutedEventArgs e)
        {
            if (StationCheckedAsSource != null)
            {
                StationCheckedAsSource(this, e);
            }
        }

        private void chkDestination_Checked(object sender, RoutedEventArgs e)
        {
            if (StationCheckedAsDestination != null)
            {
                StationCheckedAsDestination(this, e);
            }
        }

        private void chkSource_Unchecked(object sender, RoutedEventArgs e)
        {
            if (StationUncheckedAsSource != null)
            {
                StationUncheckedAsSource(this, e);
            }
        }

        private void chkDestination_Unchecked(object sender, RoutedEventArgs e)
        {
            if (StationUncheckedAsDestination != null)
            {
                StationUncheckedAsDestination(this, e);
            }
        }
    }
}
