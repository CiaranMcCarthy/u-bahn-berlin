﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;

// The Templated Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234235

namespace UBahnTravelTimer.Controls
{
    public sealed class StationToggleButton : ToggleButton
    {
        public static readonly DependencyProperty StationNameProperty = DependencyProperty.RegisterAttached(nameof(StationName), typeof(string), typeof(StationToggleButton), new PropertyMetadata(null));
        public static readonly DependencyProperty TextAlignmentProperty = DependencyProperty.RegisterAttached(nameof(TextAlignment), typeof(TextAlignment), typeof(StationToggleButton), new PropertyMetadata(TextAlignment.Left));


        public StationToggleButton()
        {
            this.DefaultStyleKey = typeof(StationToggleButton);
        }

        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public string StationName
        {
            get { return (string)GetValue(StationNameProperty); }
            set { SetValue(StationNameProperty, value); }
        }
    }
}
