﻿using System.Linq;
using UBahnNetwork;
using UBahnTravelTimer.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace UBahnTravelTimer.Controls
{
    public sealed partial class RouteControl : UserControl
    {
        #region Dependency properties
        public static readonly DependencyProperty RouteProperty = DependencyProperty.RegisterAttached(nameof(Route), typeof(Route), typeof(RouteControl), new PropertyMetadata(null, ItemsSourceProperty_PropertyChanged));
        public static readonly DependencyProperty SelectedSourceStationProperty = DependencyProperty.RegisterAttached(nameof(SelectedSourceStation), typeof(Station), typeof(RouteControl), new PropertyMetadata(null));
        public static readonly DependencyProperty SelectedDestinationStationProperty = DependencyProperty.RegisterAttached(nameof(SelectedDestinationStation), typeof(Station), typeof(RouteControl), new PropertyMetadata(null));


        private static void ItemsSourceProperty_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RouteControl RouteControl = (RouteControl)d;
            Route route = (Route)e.NewValue;
            RouteControl.LoadStations(route);
        }
        #endregion

        public RouteControl()
        {
            this.InitializeComponent();
        }

        public Route Route
        {
            get { return (Route)GetValue(RouteProperty); }
            set { SetValue(RouteProperty, value); }
        }

        public Station SelectedSourceStation
        {
            get { return (Station)GetValue(SelectedSourceStationProperty); }
            set { SetValue(SelectedSourceStationProperty, value); }
        }

        public Station SelectedDestinationStation
        {
            get { return (Station)GetValue(SelectedDestinationStationProperty); }
            set { SetValue(SelectedDestinationStationProperty, value); }
        }

        private void LoadStations(Route route)
        {
            MainPanel.Children.Clear();
            for (int index = 0; index < route.Stations.Length; index++)
            {
                Station station = route.Stations[index];
                StopControl StopControl = new StopControl()
                {
                    DataContext = station,
                    FirstRouteBrush = RouteViewModel.GetRouteColorBrush(route.FirstColourHex),
                    SecondRouteBrush = RouteViewModel.GetRouteColorBrush(route.SecondColourHex),
                    IsLast = (index >= route.Stations.Length - 1)
                };
                StopControl.StationCheckedAsSource += StopControl_StationCheckedAsSource;
                StopControl.StationCheckedAsDestination += StopControl_StationCheckedAsDestination;
                StopControl.StationUncheckedAsSource += StopControl_StationUncheckedAsSource;
                StopControl.StationUncheckedAsDestination += StopControl_StationUncheckedAsDestination;

                MainPanel.Children.Add(StopControl);
            }
        }

        private void StopControl_StationUncheckedAsSource(object sender, RoutedEventArgs e)
        {
            SelectedSourceStation = null;
        }

        private void StopControl_StationUncheckedAsDestination(object sender, RoutedEventArgs e)
        {
            SelectedDestinationStation = null;
        }

        private void StopControl_StationCheckedAsSource(object sender, RoutedEventArgs e)
        {
            foreach (StopControl child in MainPanel.Children.Where(i => i != sender && ((StopControl)i).IsCheckedAsSource == true))
            {
                child.IsCheckedAsSource = false;
            }
            StopControl senderStopControl = (StopControl)sender;
            SelectedSourceStation = senderStopControl.Station;
        }

        private void StopControl_StationCheckedAsDestination(object sender, RoutedEventArgs e)
        {
            foreach (StopControl child in MainPanel.Children.Where(i => i != sender && ((StopControl)i).IsCheckedAsDestination == true))
            {
                child.IsCheckedAsDestination = false;
            }
            StopControl senderStopControl = (StopControl)sender;
            SelectedDestinationStation = senderStopControl.Station;
        }
    }
}
