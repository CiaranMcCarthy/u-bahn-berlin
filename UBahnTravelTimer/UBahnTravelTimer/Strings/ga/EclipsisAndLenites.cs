﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBahnTravelTimer.Strings.ga
{
    public static class EclipsisAndLenites
    {
        public static string ApplyGrammarToString(string input, Modifications modification)
        {
            switch (modification)
            {
                case Modifications.Lenite:
                    return Lenite(input);

                case Modifications.Eclipsis:
                    return Eclipsis(input);

                default:
                    return input;
            }
        }

        private static string Eclipsis(string input)
        {
            switch (input[0])
            {
                case 'b':
                case 'B':
                    input = AddEclipsis(input, "m");
                    break;

                case 'c':
                case 'C':
                    input = AddEclipsis(input, "g");
                    break;

                case 'd':
                case 'D':
                case 'g':
                case 'G':
                    input = AddEclipsis(input, "n");
                    break;

                case 'f':
                case 'F':
                    input = AddEclipsis(input, "bh");
                    break;

                case 'p':
                case 'P':
                    input = AddEclipsis(input, "b");
                    break;

                case 't':
                case 'T':
                    input = AddEclipsis(input, "d");
                    break;

                case 'a':
                case 'á':
                case 'e':
                case 'é':
                case 'i':
                case 'í':
                case 'o':
                case 'ó':
                case 'u':
                case 'ú':
                    input = AddEclipsis(input, "n-");
                    break;

                case 'A':
                case 'Á':
                case 'E':
                case 'É':
                case 'I':
                case 'Í':
                case 'O':
                case 'Ó':
                case 'U':
                case 'Ú':
                    input = AddEclipsis(input, "n");
                    break;
            }
            return input;
        }

        private static string AddEclipsis(string input, string eclipsis)
        {
            return eclipsis + input;
        }

        private static string Lenite(string input)
        {
            switch (input.ToUpperInvariant()[0])
            {
                case 'B':
                case 'C':
                case 'D':
                case 'F':
                case 'G':
                case 'M':
                case 'P':
                case 'T':
                    input = AddLentitionToWord(input);
                    break;

                case 'S':
                    if (input[1] != 'C' && input[1] != 'M' && input[1] != 'P' && input[1] != 'T')
                    {
                        input = AddLentitionToWord(input);
                    }
                    break;
            }
            return input;
        }

        private static string AddLentitionToWord(string input)
        {
            List<char> letters = input.ToCharArray().ToList();
            letters.Insert(1, 'h');
            return new string(letters.ToArray());
        }
    }

    public enum Modifications
    {
        None,
        Lenite,
        Eclipsis
    }
}
