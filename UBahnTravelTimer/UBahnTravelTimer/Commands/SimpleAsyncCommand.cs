﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace UBahnTravelTimer.Commands
{
    public class SimpleAsyncCommand : ICommand
    {
        #region Fields
        public event EventHandler CanExecuteChanged;

        private Action _preExecute;
        private Func<Task> _execute;
        private Action<Task> _postExecute;
        #endregion

        #region Constructor(s)
        public SimpleAsyncCommand(Action preExecute, Func<Task> execute, Action<Task> postExecute)
        {
            _preExecute = preExecute;
            _execute = execute;
            _postExecute = postExecute;
        }
        #endregion

        #region Methods
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public async void Execute(object parameter)
        {
            _preExecute();

            await _execute().ContinueWith((t) =>
            {
                _postExecute(t);
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
        #endregion
    }
}
