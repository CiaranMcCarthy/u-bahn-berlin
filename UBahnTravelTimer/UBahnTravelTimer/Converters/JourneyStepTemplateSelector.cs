﻿using UBahnNetwork.Journeys;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace UBahnTravelTimer.Converters
{
    public class JourneyStepTemplateSelector : DataTemplateSelector
    {
        public DataTemplate RouteJourneyStepDataTemplate { get; set; }
        public DataTemplate StationJourneyStepDataTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item)
        {
            if (item is RouteJourneyStep)
            {
                return RouteJourneyStepDataTemplate;
            }
            else if (item is StationJourneyStep)
            {
                return StationJourneyStepDataTemplate;
            }
            else
            {
                return base.SelectTemplateCore(item);
            }
        }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            return SelectTemplateCore(item);
        }
    }
}
