﻿using System;
using UBahnNetwork;
using UBahnTravelTimer.ViewModels;
using Windows.UI.Xaml.Data;

namespace UBahnTravelTimer.Converters
{
    public class RouteToRouteViewModelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            Route route = (Route)value;
            return new RouteViewModel(route);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            RouteViewModel viewModel = (RouteViewModel)value;
            return viewModel.Route;
        }
    }
}
