﻿using System;
using Windows.UI.Xaml.Data;

namespace UBahnTravelTimer.Converters
{
    public class DateTimeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            DateTime? dateTime = value as DateTime?;
            if (dateTime == null)
            {
                return string.Empty;
            }
            
            string stringParameter = (string)parameter;
            return dateTime.Value.ToString(stringParameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
