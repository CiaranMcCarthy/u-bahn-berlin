﻿using System;
using Windows.UI.Xaml.Data;

namespace UBahnTravelTimer.Converters
{
    public class MinutesToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            TimeSpan? timeSpan = value as TimeSpan?;
            if (timeSpan == null)
            {
                return null;
            }

            return timeSpan.Value.TotalMinutes;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
