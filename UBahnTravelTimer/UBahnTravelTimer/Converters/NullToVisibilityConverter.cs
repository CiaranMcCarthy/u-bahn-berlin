﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace UBahnTravelTimer.Converters
{
    public class NullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            bool isNull = (value != null);
            if (parameter is bool)
            {
                bool invert = (bool)parameter;
                isNull = invert ? !isNull : isNull;
            }

            return (isNull) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
