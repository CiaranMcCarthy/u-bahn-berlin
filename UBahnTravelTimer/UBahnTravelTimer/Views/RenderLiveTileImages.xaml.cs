﻿using System;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using UBahnNetwork;
using UBahnTravelTimer.ViewModels;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace UBahnTravelTimer.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RenderLiveTileImages : Page
    {
        private int _currentIndex;
        static SupportedRoutes[] Routes;
        static RenderLiveTileImages()
        {
            Routes = (SupportedRoutes[])Enum.GetValues(typeof(SupportedRoutes));
        }


        public RenderLiveTileImages()
        {
            this.InitializeComponent();
        }

        private async void abbSave_Click(object sender, RoutedEventArgs e)
        {
            RenderTargetBitmap renderTargetBitmap = new RenderTargetBitmap();
            await renderTargetBitmap.RenderAsync(RenderedGrid);

            IBuffer pixelBuffer = await renderTargetBitmap.GetPixelsAsync();
            byte[] pixels = pixelBuffer.ToArray();


            StorageFolder folder = Windows.Storage.KnownFolders.PicturesLibrary;
            StorageFile file = await folder.CreateFileAsync(Model.Route.RouteNumber + ".png", CreationCollisionOption.ReplaceExisting);
            using (var stream = await file.OpenAsync(FileAccessMode.ReadWrite))
            {
                BitmapEncoder encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, stream);
                encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore, (uint)renderTargetBitmap.PixelWidth, (uint)renderTargetBitmap.PixelHeight, 96, 96, pixels);
                await encoder.FlushAsync();
            }

            _currentIndex++;
            if (_currentIndex >= Routes.Length)
            {
                App.Current.Exit();
            }

            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(RenderLiveTileImages), _currentIndex);
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            _currentIndex = (int)e.Parameter;

            Route route = await Route.LoadAsync(Routes[_currentIndex]);
            Model = new RouteViewModel(route);
        }

        public RouteViewModel Model
        {
            get
            {
                return (RouteViewModel)DataContext;
            }
            set
            {
                DataContext = value;
            }
        }
    }
}
