﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UBahnNetwork;
using UBahnTravelTimer.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace UBahnTravelTimer.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RoutesPage : Page
    {
        private SupportedRoutes? _selectedRoute;
        public RoutesPage()
        {
            this.InitializeComponent();
            var resourceLoader = Windows.ApplicationModel.Resources.ResourceLoader.GetForCurrentView();
            this.tblTitle.Text = resourceLoader.GetString("Name");
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // Load the network
            await UBahnNetwork.UBahnNetwork.InitialiseAsync();

            if (_selectedRoute != null)
            {
                Frame.Navigate(typeof(RoutePage), _selectedRoute.Value);
                return;
            }

            IEnumerable<RouteViewModel> allRoutes = UBahnNetwork.UBahnNetwork.Network.AllRoutes.Select(i => new RouteViewModel(i));
            this.DataContext = allRoutes;
        }

        private void route_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement frameworkElement = (FrameworkElement)sender;
            Frame.Navigate(typeof(RoutePage), frameworkElement.Tag);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (e.Parameter is SupportedRoutes && e.NavigationMode != NavigationMode.Back)
            {
                _selectedRoute = (SupportedRoutes)e.Parameter;
            }
        }

        private void abbJourneyPlanner_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(JourneyPage));
        }
    }
}
