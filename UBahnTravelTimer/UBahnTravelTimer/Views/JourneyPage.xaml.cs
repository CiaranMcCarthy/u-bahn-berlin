﻿using System;
using UBahnTravelTimer.ViewModels;
using Windows.ApplicationModel.Resources;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace UBahnTravelTimer.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class JourneyPage : Page
    {
        private ResourceLoader _resourceLoader;

        public JourneyPage()
        {
            this.InitializeComponent();

            _resourceLoader = Windows.ApplicationModel.Resources.ResourceLoader.GetForCurrentView();

            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            SystemNavigationManager.GetForCurrentView().BackRequested += OnBackRequested;

            this.ViewModel = new JourneyViewModel();
            this.ViewModel.PropertyChanged += ViewModel_PropertyChanged;
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame.CanGoBack)
            {
                e.Handled = true;
                rootFrame.GoBack();
            }
        }

        private async void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is IErrorViewModel == false)
            {
                return;
            }

            IErrorViewModel errorViewModel = (IErrorViewModel)sender;
            if (e.PropertyName == nameof(IErrorViewModel.Error))
            {
                if (errorViewModel.Error != null)
                {
                    string errorMessage = string.Format(_resourceLoader.GetString("ErrorMessageFormatString"), errorViewModel.Error);
                    MessageDialog dialog = new MessageDialog(errorMessage);
                    await dialog.ShowAsync();
                }
            }
        }

        private JourneyViewModel ViewModel
        {
            get
            {
                return (JourneyViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
