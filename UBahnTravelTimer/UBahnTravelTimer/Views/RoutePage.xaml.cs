﻿using System;
using UBahnNetwork;
using UBahnTravelTimer.Strings.ga;
using UBahnTravelTimer.ViewModels;
using Windows.ApplicationModel.DataTransfer;
using Windows.ApplicationModel.Resources;
using Windows.UI.Core;
using Windows.UI.StartScreen;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace UBahnTravelTimer.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RoutePage : Page
    {
        private ResourceLoader _resourceLoader;

        public RoutePage()
        {
            this.InitializeComponent();
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            SystemNavigationManager.GetForCurrentView().BackRequested += OnBackRequested;

            DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
            dataTransferManager.DataRequested += DataTransferManager_DataRequested;

            _resourceLoader = Windows.ApplicationModel.Resources.ResourceLoader.GetForCurrentView();
        }

        private void DataTransferManager_DataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            DataRequest request = args.Request;

            if (Model.FirstSelectedStation == null || Model.SecondSelectedStation == null || Model.ArrivalTime == null)
            {
                request.FailWithDisplayText(_resourceLoader.GetString("SelectTwoStationsToShare"));
                return;
            }

            string startingStation = Model.FirstSelectedStation.Name;
            string destinationStation = Model.SecondSelectedStation.Name;

            if (Windows.System.UserProfile.GlobalizationPreferences.Languages[0].EndsWith("ga"))
            {
                startingStation = EclipsisAndLenites.ApplyGrammarToString(startingStation, Modifications.Eclipsis);
                destinationStation = EclipsisAndLenites.ApplyGrammarToString(destinationStation, Modifications.Eclipsis);
            }


            string text = string.Format(_resourceLoader.GetString("SharingTimesMessageFormatString"), startingStation, destinationStation, Model.ArrivalTime.Value.ToString("HH:mm"));
            request.Data.SetText(text);
            request.Data.Properties.Title = _resourceLoader.GetString("Name");
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame.CanGoBack)
            {
                e.Handled = true;
                rootFrame.GoBack();
            }
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            Route route = await Route.LoadAsync((SupportedRoutes)e.Parameter);
            Model = new RouteViewModel(route);
        }

        public RouteViewModel Model
        {
            get
            {
                return (RouteViewModel)DataContext;
            }
            set
            {
                DataContext = value;
            }
        }

        private void abbShare_Click(object sender, RoutedEventArgs e)
        {
            DataTransferManager.ShowShareUI();
        }

        private async void abbPin_Click(object sender, RoutedEventArgs e)
        {
            // Prepare package images for use as the Tile Logo in our tile to be pinned.
            var uriLogo = new Uri(string.Format("ms-appx:///Assets/Tiles/{0}.png", Model.Route.RouteNumber));

            string arguments = Model.Route.RouteNumber.ToString();
            var secondaryTile = new SecondaryTile(Model.Route.RouteNumber.ToString(), Model.Route.RouteNumber.ToString(), arguments, uriLogo, TileSize.Default) { RoamingEnabled = true };
            await secondaryTile.RequestCreateAsync();
        }
    }
}
